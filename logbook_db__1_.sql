-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2022 at 10:12 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logbook_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `kendala`
--

CREATE TABLE `kendala` (
  `id_kendala` int(7) NOT NULL,
  `laboran` varchar(35) NOT NULL,
  `date` date NOT NULL,
  `lab` varchar(7) NOT NULL,
  `pc` varchar(5) NOT NULL,
  `kendala` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kendala`
--

INSERT INTO `kendala` (`id_kendala`, `laboran`, `date`, `lab`, `pc`, `kendala`) VALUES
(1, 'dinzyyy', '2022-01-17', 'lab-322', 'PC-19', 'bismillah semoga kartika berhasil'),
(8, 'saya', '2022-01-16', 'lab-333', 'PC-14', 'jhadjaksjdsbajkcjsbkbjk'),
(9, 'baymax', '2022-02-08', '227', 'PC-14', 'perlu dibanting');

-- --------------------------------------------------------

--
-- Table structure for table `kendala2`
--

CREATE TABLE `kendala2` (
  `id_kendala` int(7) NOT NULL,
  `laboran` varchar(35) NOT NULL,
  `date` date NOT NULL,
  `lab` varchar(7) NOT NULL,
  `pc` varchar(5) NOT NULL,
  `kendala` varchar(200) NOT NULL,
  `level` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kendala2`
--

INSERT INTO `kendala2` (`id_kendala`, `laboran`, `date`, `lab`, `pc`, `kendala`, `level`) VALUES
(3, 'dinzyyy', '2022-01-17', 'lab-322', 'PC-19', 'bismillah semoga kartika berhasil', 'level-2'),
(4, 'saya', '2022-01-16', 'lab-333', 'PC-14', 'jhadjaksjdsbajkcjsbkbjk', 'level-3'),
(5, 'dinzyyy', '2022-01-17', 'lab-322', 'PC-19', 'bismillah semoga kartika berhasil', 'level-2'),
(6, 'baymax', '2022-02-08', '227', 'PC-14', 'perlu dibanting', 'level-2');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `level` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`level`) VALUES
('Level-1'),
('Level-2'),
('Level-3');

-- --------------------------------------------------------

--
-- Table structure for table `peminjamanlab`
--

CREATE TABLE `peminjamanlab` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `tgl_pengajuan` varchar(20) NOT NULL,
  `ruang` varchar(10) NOT NULL,
  `tgl_peminjaman` varchar(20) NOT NULL,
  `tgl_pengembalian` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peminjamanlab`
--

INSERT INTO `peminjamanlab` (`id`, `nama`, `nim`, `tgl_pengajuan`, `ruang`, `tgl_peminjaman`, `tgl_pengembalian`, `status`) VALUES
('PL03', 'Bimaaa', '2055301033', '2022-02-07', 'Lab 329', '2022-02-09', '2022-02-11', 'Digunakan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kendala`
--
ALTER TABLE `kendala`
  ADD PRIMARY KEY (`id_kendala`);

--
-- Indexes for table `kendala2`
--
ALTER TABLE `kendala2`
  ADD PRIMARY KEY (`id_kendala`);

--
-- Indexes for table `peminjamanlab`
--
ALTER TABLE `peminjamanlab`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kendala`
--
ALTER TABLE `kendala`
  MODIFY `id_kendala` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `kendala2`
--
ALTER TABLE `kendala2`
  MODIFY `id_kendala` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
